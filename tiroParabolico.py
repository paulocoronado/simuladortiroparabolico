from graphics import *
from math import sin, cos, radians
from time import sleep

Vo=80
t=0
angulo=70
angulo=angulo*3.1415926535/180

y=0
g=9.8

while (y>=0):
    x=Vo*cos(angulo)*t
    y=Vo*sin(angulo)*t -(1.0/2)*g*t*t
    print(x,y)
    sleep(0.1)
    t=t+0.5
