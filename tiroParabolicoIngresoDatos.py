from graphics import *
from math import sin, cos, radians
from time import sleep

ancho=700
largo=700
ventana = GraphWin("Movimiento Parabolico", ancho, largo)
ventana.setCoords(0,0,ancho,largo)
textoVo = Entry(Point(50,300), 5)
textoVo.draw(ventana)
textoAngulo = Entry(Point(50,250), 5)
textoAngulo.draw(ventana)

ventana.getMouse()
angulo=int(textoAngulo.getText())*3.1415926535/180
Vo=int(textoVo.getText())
t=0
y=0
g=9.8

while (y>=0):
    x=Vo*cos(angulo)*t
    y=Vo*sin(angulo)*t -(1.0/2)*g*t*t
    miCirculo=Circle(Point(x,y),10)
    miCirculo.setFill("yellow")
    miCirculo.draw(ventana)
    sleep(0.1)
    t=t+0.5


ventana.getMouse()
ventana.close()
